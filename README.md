
## Pingbot

### What is it

Pingbot is a small slack "bot" that pings servers and sends a message
to slack if a server is unreachable.

### Requirements

* python3
* pip
* Linux enviroment for the ping command (changing to Windows equivalent
  should be trivial)
* [slack legacy token](https://api.slack.com/custom-integrations/legacy-tokens)

### Installing and running

To install pingbot, download/clone this repo and run

```pip install -r requirements.txt```

This will install the needed python libraries.

Next create a file called `token.txt` (in the same directory as pingbot.py)
and paste your slack token in the file.

To run our script we use a crontab like this

```*/5 8-16 * * 1-5 /path/to/pingbot.py```

Check out [crontab.guru](https://crontab.guru/) for easy crontab creation


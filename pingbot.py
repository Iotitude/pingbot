#! /usr/bin/env python3
#
# USAGE
# =====
#
# Paste your slack legacy token in a file named 'token.txt' next to pingbot.py
# -- OR--
# Run pingbot with the token in an environment variable:
#
# $ PINGBOT_TOKEN="YOUR_TOKEN_HERE" pingbot.py
#

import os
from pathlib import Path
import random
from subprocess import DEVNULL, STDOUT, call
import sys

from slackclient import SlackClient


class Server:
    def __init__(self, ip, name):
        self.ip = ip
        self.name = name

    def ping(self):
        return call(["ping", "-c", "2", "-w", "2", self.ip], stdout=DEVNULL,
                    stderr=STDOUT) == 0


slack_channel = "#it_hurts_when_ip"

servers_to_check = [
    Server("192.168.39.11", "11 Cassandra"),
    Server("192.168.39.12", "12 Cassandra"),
    Server("192.168.39.13", "13 Cassandra"),
    Server("192.168.39.14", "14 Cassandra"),
    Server("192.168.39.15", "15 MariaDB"),
    Server("192.168.39.16", "16 MariaDB"),
    Server("192.168.39.17", "17 MariaDB"),
    Server("192.168.39.18", "18 Docker"),
    Server("192.168.39.19", "19 Docker"),
    Server("192.168.39.20", "20 Docker"),
    Server("192.168.39.21", "21 Docker"),
    Server("192.168.39.23", "23 Docker"),
    Server("192.168.39.24", "24 Docker"),
]

emojis = [
    ":psyduck:",
    ":psyduck2:",
    ":facepalm:",
    ":sonic:",
    ":shipit:",
    ":hankey:",
    ":crying_cat_face:",
    ":frog:",
]

death_messages = [
    "assumed room temperature.",
    "bit the dust.",
    "is as dead as a doornail.",
    "didn't make it.",
    "dropped like a fly.",
    "dropped dead.",
    "is worm food.",
    "gave up the ghost.",
    "kicked the bucket.",
    "went to live on a farm upstate.",
    "made the ultimate sacrifice.",
    "met their maker.",
    "passed away.",
    "is pushing up daisies.",
    "is six feet under.",
    "sleeps with the fishes.",
]


def get_script_path():
    return os.path.dirname(os.path.realpath(__file__))


def get_token_path():
    return os.path.join(get_script_path(), "token.txt")


def get_token_file():
    token_file = Path(get_token_path())
    if token_file.is_file():
        print("Token file found")
        token = "test"
        with token_file.open() as f:
            token = f.readline()
        return token
    print("Token file not found")
    return None


def get_api_token():
    token = get_token_file()
    if not token:
        try:
            print("Trying env variable")
            token = os.environ["IPBOT_TOKEN"]
        except KeyError as error:
            print("No environment variable found :(")
            print(error)
            print("No slack api token found!\nCannot continue!")
            sys.exit(1)
    return token.rstrip()


slack_token = get_api_token()
sc = SlackClient(slack_token)

print(slack_token)


def post_to_slack(server):
    message = "{} {} {}".format(server.name, random.choice(death_messages),
                                random.choice(emojis))
    sc.api_call(
        "chat.postMessage",
        channel=slack_channel,
        text=message
    )


# def ping(server):
#     ip = server.ip
#     return call(["ping", "-c", "2", "-w", "2", ip], stdout=DEVNULL,
#                 stderr=STDOUT) == 0


def check_servers():
    for server in servers_to_check:
        if not server.ping():
            post_to_slack(server)
            break


if __name__ == "__main__":
    check_servers()
